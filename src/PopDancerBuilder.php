<?php

/**
 * Created by PhpStorm.
 * User: rail
 * Date: 17.04.17
 * Time: 23:21
 */
class PopDancerBuilder extends DancerBuilder
{

    public function buildHandAction()
    {
        $this->_dancer->setHandAction('Плавное движение руками');
    }

    public function buildHeadAction()
    {
        $this->_dancer->setHeadAction('Плавное движение головой');
    }

    public function buildLegAction()
    {
        $this->_dancer->setLegAction('Плавное движение ногами');
    }

    public function buildBodyAction()
    {
        $this->_dancer->setBodyAction('Плавное движение туловищем');
    }
}