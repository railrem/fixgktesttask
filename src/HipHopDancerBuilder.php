<?php

/**
 * Created by PhpStorm.
 * User: rail
 * Date: 17.04.17
 * Time: 23:21
 */
class HipHopDancerBuilder extends DancerBuilder
{

    public function buildHandAction()
    {
        $this->_dancer->setHandAction('Согнуть локти');
    }

    public function buildHeadAction()
    {
        $this->_dancer->setHeadAction('Двигать головой вперед назад');
    }

    public function buildLegAction()
    {
        $this->_dancer->setLegAction('Ноги в полуприсяд');
    }

    public function buildBodyAction()
    {
        $this->_dancer->setBodyAction('Туловище вперед и назад');
    }
}