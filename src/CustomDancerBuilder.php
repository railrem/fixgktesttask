<?php

/**
 * Created by PhpStorm.
 * User: rail
 * Date: 17.04.17
 * Time: 23:41
 */
class CustomDancerBuilder extends DancerBuilder
{

    public $handAction;
    public $headAction;
    public $legAction;
    public $bodyAction;

    /**
     * CustomDancerBuilder constructor.
     * @param $handAction
     * @param $headAction
     * @param $legAction
     * @param $bodyAction
     */
    public function __construct($handAction, $headAction, $legAction, $bodyAction)
    {
        $this->handAction = $handAction;
        $this->headAction = $headAction;
        $this->legAction = $legAction;
        $this->bodyAction = $bodyAction;
    }


    public function buildHandAction()
    {
        $this->_dancer->setHandAction($this->handAction);
    }


    public function buildHeadAction()
    {
        $this->_dancer->setHeadAction($this->headAction);
    }

    public function buildLegAction()
    {
        $this->_dancer->setLegAction($this->legAction);
    }

    public function buildBodyAction()
    {
        $this->_dancer->setBodyAction($this->bodyAction);
    }
}