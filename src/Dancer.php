<?php

/**
 * Created by PhpStorm.
 * User: rail
 * Date: 17.04.17
 * Time: 22:31
 */
class Dancer
{
    public $name;

    private $_handAction;

    private $_legAction;

    private $_bodyAction;

    private $_headAction;

    public function drink()
    {
        return 'Пить водку';
    }

    public function dance()
    {
        return $this->_headAction
        . ' '
        . $this->_handAction
        . ' '
        . $this->_bodyAction
        . ' '
        . $this->_legAction;
    }

    /**
     * @param $dancer Dancer
     * @return bool
     */
    public function сompare($dancer)
    {
        if (!($dancer instanceof Dancer)) {
            return false;
        }
        if ($this->_handAction == $dancer->_handAction && $this->_legAction == $dancer->_legAction
            && $this->_bodyAction == $dancer->_bodyAction && $this->_headAction == $dancer->_headAction
        ) {
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getHandAction()
    {
        return $this->_handAction;
    }

    /**
     * @param mixed $_handAction
     */
    public function setHandAction($_handAction)
    {
        $this->_handAction = $_handAction;
    }

    /**
     * @return mixed
     */
    public function getLegAction()
    {
        return $this->_legAction;
    }

    /**
     * @param mixed $_legAction
     */
    public function setLegAction($_legAction)
    {
        $this->_legAction = $_legAction;
    }

    /**
     * @return mixed
     */
    public function getBodyAction()
    {
        return $this->_bodyAction;
    }

    /**
     * @param mixed $_bodyAction
     */
    public function setBodyAction($_bodyAction)
    {
        $this->_bodyAction = $_bodyAction;
    }

    /**
     * @return mixed
     */
    public function getHeadAction()
    {
        return $this->_headAction;
    }

    /**
     * @param mixed $_headAction
     */
    public function setHeadAction($_headAction)
    {
        $this->_headAction = $_headAction;
    }

}