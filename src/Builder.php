<?php

/**
 * Created by PhpStorm.
 * User: rail
 * Date: 17.04.17
 * Time: 23:47
 */
class Builder
{
    /** @var  $_dancerBuilder DancerBuilder */
    private $_dancerBuilder;

    /**
     * @param $dancerBuilder
     */
    public function setDancerBuilder($dancerBuilder)
    {
        $this->_dancerBuilder = $dancerBuilder;
    }

    /**
     * @return Dancer
     */
    public function getDancer()
    {
        return $this->_dancerBuilder->getDancer();
    }

    /**
     * inizialize dancer object
     */
    public function constructDancer()
    {
        $this->_dancerBuilder->createNewDancer();
        $this->_dancerBuilder->buildBodyAction();
        $this->_dancerBuilder->buildHandAction();
        $this->_dancerBuilder->buildHeadAction();
        $this->_dancerBuilder->buildLegAction();
    }

}