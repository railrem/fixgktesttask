<?php

/**
 * Created by PhpStorm.
 * User: rail
 * Date: 17.04.17
 * Time: 23:21
 */
class ElectroDancerBuilder extends DancerBuilder
{

    public function buildHandAction()
    {
        $this->_dancer->setHandAction('Вращение рукой');
    }

    public function buildHeadAction()
    {
        $this->_dancer->setHeadAction('Не двигать головой');
    }

    public function buildLegAction()
    {
        $this->_dancer->setLegAction('Движение в ритм');
    }

    public function buildBodyAction()
    {
        $this->_dancer->setBodyAction('Туловище вперед и назад');
    }
}