<?php

/**
 * Created by PhpStorm.
 * User: rail
 * Date: 17.04.17
 * Time: 22:49
 */
abstract class DancerBuilder
{
    /** @var  Dancer */
    protected $_dancer;

    public function getDancer()
    {
        return $this->_dancer;
    }

    public function createNewDancer()
    {
        $this->_dancer = new Dancer();
    }

    abstract public function buildHandAction();

    abstract public function buildHeadAction();

    abstract public function buildLegAction();

    abstract public function buildBodyAction();

}