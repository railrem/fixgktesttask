<?php
require('src/Dancer.php');
require('src/Builder.php');
require('src/DancerBuilder.php');
require('src/HipHopDancerBuilder.php');
require('src/ElectroDancerBuilder.php');
require('src/PopDancerBuilder.php');
require('src/CustomDancerBuilder.php');


$headActions = ['Не двигать головой', 'Плавное движение головой', 'Двигать головой вперед назад'];
$bodyActions = ['Туловище вперед и назад', 'Плавное движение туловищем'];
$legActions = ['Движение в ритм', 'Ноги в полуприсяд', 'Плавное движение ногами'];
$handActions = ['Вращение рукой', 'Согнуть локти', 'Плавное движение руками'];


$musics = ['hip-hop', 'electro', 'pop'];

$visitorCount = 100;
$musicCount = 6;
$customDancers = [];
$playlist = [];
$builder = new Builder();

$builderHipHopDancer = new HipHopDancerBuilder();
$builderElectroDancer = new ElectroDancerBuilder();
/** @var Dancer $builderPopDancer */
$builderPopDancer = new PopDancerBuilder();

$builder->setDancerBuilder($builderHipHopDancer);
$builder->constructDancer();
/** @var Dancer $hipHopDancer */
$hipHopDancer = $builder->getDancer();

$builder->setDancerBuilder($builderElectroDancer);
$builder->constructDancer();
/** @var Dancer $electroDancer */
$electroDancer = $builder->getDancer();

$builder->setDancerBuilder($builderPopDancer);
$builder->constructDancer();
$popDancer = $builder->getDancer();

for ($i = 0; $i < $visitorCount; $i++) {
    $builderDancer = new CustomDancerBuilder(
        $handActions[array_rand($handActions)],
        $headActions[array_rand($headActions)],
        $legActions[array_rand($legActions)],
        $bodyActions[array_rand($bodyActions)]
    );
    $builder->setDancerBuilder($builderDancer);
    $builder->constructDancer();
    $dancer = $builder->getDancer();
    $dancer->name = 'dancer' . $i;
    $customDancers[] = $dancer;
}

for ($i = 0; $i < $musicCount; $i++) {
    $playlist[] = $musics[array_rand($musics)];
}

echo 'Туса стартует !' . PHP_EOL;
foreach ($playlist as $track) {
    echo 'Новый трэк '.$track.PHP_EOL;
    /** @var Dancer $dancer */
    foreach ($customDancers as $dancer) {
        if (($track == 'hip-hop' && $hipHopDancer->сompare($dancer))
            || ($track == 'electro' && $electroDancer->сompare($dancer))
            || ($track == 'pop' && $popDancer->сompare($dancer))
        ) {
            $action = $dancer->dance();
        } else {
            $action = $dancer->drink();
        }
        echo $dancer->name . ' ' . $action . PHP_EOL;
    }
}

